//
//  AObject.m
//  Awe
//
//  Created by zcr on 2020/5/9.
//  Copyright © 2020 hundsun. All rights reserved.
//

#import "AObject.h"

@implementation AObject

- (id)alwaysNil {
    return [NSObject new];
}

@end
