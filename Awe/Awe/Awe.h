//
//  Awe.h
//  Awe
//
//  Created by zcr on 2020/5/9.
//  Copyright © 2020 hundsun. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for Awe.
FOUNDATION_EXPORT double AweVersionNumber;

//! Project version string for Awe.
FOUNDATION_EXPORT const unsigned char AweVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Awe/PublicHeader.h>

#import <Awe/AObject.h>
