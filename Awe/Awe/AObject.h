//
//  AObject.h
//  Awe
//
//  Created by zcr on 2020/5/9.
//  Copyright © 2020 hundsun. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AObject : NSObject

- (id)alwaysNil;

@end

NS_ASSUME_NONNULL_END
